# Kuberneties-example



## Getting started

1. How to pick a container to start from?
   a. is there an official container (https://docs.docker.com/docker-hub/official_images/ or pointed to from the vendor)
   b. Does the container source have a Dockerfile that you can inspect

    -  Check the From of the dockerfile, see if the container is coming from a trusted base.
        - Something thin like Alpine Linux (https://www.alpinelinux.org/) is a good start
    -  check that copy not ADD is used. (Add could source from anywhere on the internet and make it hard to audit the source)
    -  check that the build file updates base environment (yum/dnf/apt update)
    -  check for a container user, so set to not root execution (USER directive)
    -  check for unneeded packages
    -  Look for other supicuious build practices, above is just a short set

   c. Is there a security scan of the container?  When was it last run?
    -  many repositories do scans of official images and post the results.
    -  consider running a scan with a tool (even simple tool like https://github.com/quay/clair will help)
2. from the container docs identify persistance needs.
    - The smokeping example here needs a configuration directory and a data directory.
        - here is where a good container and poor container is visible, if they don't well document 
          this need you probably have a bad container.
    - Select your volume depending on if you are using an app that well supports shared filesystem or not.
3. Networking configuration
    - 
4. building a file to have k8s deploy your application
    - Look for a template file that is close to your needs
    - In this example I needed a simple app that is exposed on port 80 and with two persistant volumes